import logo from './logo.svg';
import './App.css';
import { useState } from 'react';

function App() {
  const [primeiroNumero, setPrimeiroNumero] = useState()
  const [segundoNumero, setSegundoNumero] = useState()
  const [numeroAleatorio, setNumeroAleatorio] = useState(undefined)
 
  const calcularAleatorio = () => {
    if (primeiroNumero === segundoNumero) {
      alert("Insira números diferentes")
    } else {
      var numeroGerado;
      if (primeiroNumero < segundoNumero){
          numeroGerado = parseInt(Math.random() * (segundoNumero - primeiroNumero) + primeiroNumero)
      } else {
          numeroGerado = parseInt(Math.random() * (primeiroNumero - segundoNumero) + segundoNumero)
      }
      
      setNumeroAleatorio(String(numeroGerado))
    }
  }

  return (
    <div className="App">
      <h2>Gerador de número</h2>

      <input type="text" placeholder="Primeiro número"  onChange={ e => setPrimeiroNumero(e.target.value) } />
      <input type="text" placeholder="Segundo número" onChange={ e => setSegundoNumero(e.target.value) } />
      <br />
      {primeiroNumero && <label>Primeiro número: {primeiroNumero}</label>}
      <br />
      {segundoNumero && <label>Segundo número: {segundoNumero}</label>}
      <br />
      {numeroAleatorio ? <label>Número aleatório: {numeroAleatorio} </label> : null}
      <br />

      <button onClick={ e => calcularAleatorio()}>Gerar</button>
    </div>
  );
}

export default App;
